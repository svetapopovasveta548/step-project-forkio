    Сomposition of the project participants:

    -Popova Svetlana
    -Sidletskyi Maksym

    List of technologies used in the project:

    -HTML
    -CSS
    -SCSS
    -JS
    -Gitlab
    -Git
    -Npm
    -Gulp

    Tasks performed by the participants:

    Student №1 Sidletskyi Maksym:

    -Building a Site Header with a top menu(including a drop-down menu at low screen resolutions)
    -"People Are Talking About Fork"

    Create a dropdown menu using JavaScript, develop and include media queries from the above sections

    Student №2 Popova Svetlana:

    Layout of the following sections:

    -"Revolutionary Editor"
    -"Here is what you get"
    -"Fork Subscription Pricing"

    Development and connection of media queries of the above sections
